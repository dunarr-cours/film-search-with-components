import React, {useState} from "react";
import axios from "axios";

function SearchBar(props) {
    const onFilmLoad = props.onFilmLoad
    const [search, setSearch] = useState("")
    function handleChangeSearch(event) {
        setSearch(event.target.value)
        axios.get("http://www.omdbapi.com",
            {
                params: {
                    apikey: "6ab456f3",
                    s: event.target.value
                }
            }
        ).then(response => {
            if(response.data.Search) {
                onFilmLoad(response.data.Search)
            } else {
                onFilmLoad([])
            }
        })
    }
    return  (
        <input type="text" value={search} onChange={handleChangeSearch}/>
    )
}

export default SearchBar