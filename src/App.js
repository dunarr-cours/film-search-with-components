import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from "axios"
import CardFilm from "./CardFilm";
import SearchBar from "./SearchBar";

function App() {

    const [films, setFilms] = useState([])

    function displayFilms() {
        return films.map(film => <CardFilm filmAAfficher={film}/>)
    }
    function updateFilms(films) {
        setFilms(films)
    }
  return (
    <div className="App">
        <SearchBar onFilmLoad={updateFilms}/>
        {displayFilms()}
    </div>
  );
}

export default App;
