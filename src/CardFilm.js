import React from "react";

function CardFilm(props) {
    const film = props.filmAAfficher
    return (
        <div>
            <img src={film.Poster} alt=""/>
            <h2>{film.Title}, {film.Year}</h2>
        </div>
    )
}

export default CardFilm